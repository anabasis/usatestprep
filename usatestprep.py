#!/usr/bin/env python3
import re, requests
from bs4 import BeautifulSoup
from tabulate import tabulate
from termcolor import cprint, colored
from getpass import getpass

s = requests.Session()
header = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"}

print(colored("USATestPrep Question Retriever", "green", attrs=["bold"]))

schoolid = input("School ID: ")
username = input("Username: ")
password = getpass()

# initiate login
s.post("https://www.usatestprep.com/login.php", data={"schoolid": schoolid, "username": username, "password": password, "submit": "Submit+Query"}, headers=header)
print(colored("Logged in!", "cyan", attrs=["bold"]))

# get assignments
print(colored("Fetching assignments", "cyan", attrs=["bold"]))
assignment_data = s.get("https://www.usatestprep.com/modules/message/messages_assignments_student.php", headers=header)
assignment_soup = BeautifulSoup(assignment_data.text, "html.parser")

# print assignments
table = [] 
for tr in assignment_soup.find("table").find_all("tr"):
    data = [td.get_text(strip=True) for td in tr.find_all("td")]
    a = tr.find("a", href=re.compile(r'^/modules/assignments'))
    if len(data) == 6:
        if data[2] != "Questions - Random":
            data[2] = colored(data[2], "blue", attrs=["bold"])
        try:
            table.append([data[0], re.sub(" - Random", "", data[2]), data[5], re.sub(".*=", "", a["href"])])
        except:
            print("exception")
print(tabulate(table, headers=[colored("Due Date", attrs=["bold"]), colored("Assignment Title", attrs=["bold"]), colored("Status", attrs=["bold"]), colored("ID", attrs=["bold"])], tablefmt="fancy_grid"))

def qa_dl():
    """Downloads practice questions"""
    global question_soup
    question_data = s.get("https://www.usatestprep.com/modules/questions/question/question.php", headers=header)
    question_soup = BeautifulSoup(question_data.text, "html.parser")

def qa_fmt():
    """Formats practices questions retrieved"""
    questions = question_soup.find_all("div", class_="englishQuestion")
    answers = question_soup.find_all("span", class_="englishQuestion")
    returnval = ""

    for question in questions:
        stripped = question.getText(strip=True)
        if stripped != "Hint" and stripped != "":
            returnval += stripped + "\n"

    i = 0
    alpha = ["A) ", "B) ", "C) ", "D) ", "E) "]
    for answer in answers:
        returnval += "\t" + alpha[i] + answer.get_text(strip=True) + "\n"
        i += 1
    returnval += "\tCorrect answer: " + question_soup.find("img", src="/images/check.gif").find_previous_sibling()["value"].upper() + ")\n"

    return returnval

def qa_presenter():
    """Initializes and terminates practice questions download"""
    cprint("Retrieving questions!", "cyan")
    s.get("https://www.usatestprep.com/modules/assignments/assignment.php?id=" + testid, headers=header)
    qa_dl()
    while question_soup.find("div", class_="alert alert-danger text-center") == None and question_soup.find("div", class_="blue") == None:
        if question_soup.find("div", id="question_explanation") == None:
            qa_dl()
            file.write(qa_fmt() + "\n")
        else:
            qa_dl()
    cprint("Download ended!", "magenta")

def test_presenter():
    """Initializes and terminates test download"""
    cprint("Retrieving test questions!", "cyan")
    data = s.post("https://www.usatestprep.com/modules/test/benchmark_test.php?testid=", data={"testCode": testid}, headers=header)
    test_url = "https://www.usatestprep.com/modules/test/" + re.search("(?<=').*(?=')", data.text.partition("\n")[0])[0]

    global question_soup
    question_data = s.get(test_url, headers=header)
    question_soup = BeautifulSoup(question_data.text, "html.parser")

    file.write(test_fmt() + "\n")
    cprint("Download ended!", "magenta")

def test_fmt():
    """Formats test questions retrieved"""
    questions = question_soup.find_all("div", class_="question englishQuestion")
    returnval = ""
    for question in questions:
        stripped = question.getText(strip=True)
        if stripped != "Hint" and stripped != "":
            returnval += stripped + "\n"

        answers = question.find_parent("table").next_sibling.next_sibling.find_all("span", class_="englishQuestion")
        i = 0
        alpha = ["A) ", "B) ", "C) ", "D) ", "E) "]
        for answer in answers:
            returnval += "\t" + alpha[i] + answer.get_text(strip=True) + "\n"
            i += 1

        returnval += "\n"

    return returnval

file_name = input(colored("Where would you like to save the retrieved questions? ", "magenta"))
file = open(file_name, "a")

while (testid := input(colored("Enter practice ID, test followed by a space and test ID, or exit: ", attrs=["bold"]))) != "exit":
    if testid.__contains__("test "):
        testid = testid.replace("test ", "")
        test_presenter()
    else:
        qa_presenter()
else:
    file.close()
    print(colored("Exiting...", "red", attrs=["bold"]))
