# USATestprep Question Retriever

A simple script for downloading questions and answers from USATestprep practice assignments and tests.

USATestprep Question Retriever is licensed under the Unlicense.
